<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', 'IndexController@index');
Route::get('/index', 'IndexController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

// Categories route
// Only for admins
Route::group(['middleware' => 'CheckRole'], function () {

Route::get('/category','CategoryController@index');
Route::get('/category/create','CategoryController@create')->name('create');
Route::post('/category','CategoryController@store')->name('store');
Route::get('/category/{id}','CategoryController@show')->name('show');
Route::get('/category/{id}/edit','CategoryController@edit')->name('edit');
Route::put('/category/{id}','CategoryController@update')->name('update');
Route::delete('/category/{id}','CategoryController@destroy')->name('destroy');
});

// Posts route
Route::get('/post','PostController@index')->name('index');
Route::get('/post/create','PostController@create')->name('create');
Route::post('/post','PostController@store')->name('post_store');
Route::get('/post/{id}','PostController@show')->name('show');
Route::get('/post/{id}/edit','PostController@edit')->name('edit');
Route::put('/post/{id}','PostController@update')->name('update');
Route::delete('/post/{id}','PostController@destroy')->name('destroy');
Route::get('/post/{id}/like','PostController@like')->name('post_like'); // Is it correct ??

// Comment route
Route::get('/comment/{id}','CommentController@create');
Route::post('/comment','CommentController@store');

// Blogs route
Route::get('/blog','BlogController@index')->name('index');
Route::get('/blog/create','BlogController@create')->name('create');
Route::post('/blog','BlogController@store')->name('store');
Route::get('/blog/{id}','BlogController@show')->name('show');
Route::get('/blog/{id}/edit','BlogController@edit')->name('edit');
Route::put('/blog/{id}','BlogController@update')->name('update');
Route::delete('/blog/{id}','BlogController@destroy')->name('destroy');


// 404
Route::fallback(function(){
	return view('error.404');
});