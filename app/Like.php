<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    function users()
    {
        return $this->belongsToMany('App\User');
    }
    function posts()
    {
        return $this->belongsToMany('App\Post');
    }
}
