<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $fillable = [
    	'title', 'description',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
    public function post()
    {
    	return $this->hasMany('App\Post');
    }
}
