<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    protected $fillable = [
    	'body',
    ];

    function users()
    {
        return $this->belongsToMany('App\User');
    }
    function posts()
    {
        return $this->belongsToMany('App\Post');
    }
}
