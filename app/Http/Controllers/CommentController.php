<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Views;
use App\Comment;

class CommentController extends Controller
{
    public function create($id)
    {
    	return view('comments.create',compact('id'));
    }
    public function store(Request $request)
    {
    	$comment = new Comment();
    	$comment->body = $request->input('body');
    	$comment->post_id = $request->input('post_id');
    	$comment->user_id = auth()->user()->id;
    	$comment->save();
    	return redirect('/index')->with('success','Your comment has been posted.');
    }
}
