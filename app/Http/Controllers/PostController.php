<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Category;
use App\Blog;
use App\User;
use App\Like;
use App\Comment;

class PostController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
    public function index()
	{
		$posts = Post::get();
		return view('posts.index',compact('posts'));
	}
	public function create()
	{
		$posts = Post::get();
		$categories = Category::get();
		$user_id = auth()->user()->id;
		$blogs = Blog::get()->where('user_id',$user_id);

		return view('posts.create', compact('posts','categories','blogs'));
	}
	public function store(Request $request)
	{
		$this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'tags' => 'required',
            'category_id' => 'required',
            'blog_id' => 'required',
        ]);
		$post = new Post;
		$post->title = $request->input('title');
		$post->body = $request->input('body');
		$post->tags = $request->input('tags');
		$post->category_id = $request->input('category_id');
		$post->blog_id = $request->input('blog_id');
		$post->user_id = auth()->user()->id;
		$post->save();

		return redirect('/post/create')->with('success',"Post created successfully");
	}
	public function show($id)
	{
		$post = Post::find($id);
		$category_title = Category::find($post->category_id)->title;
		$blog_title = Blog::find($post->blog_id)->title;
		$blogger = User::find($post->user_id)->name;

		$comments = Comment::get()->where('post_id',$id);

		return view('posts.show',compact('post','category_title','blog_title','blogger','comments'));
	}
	public function edit($id)
	{
		$post = Post::find($id);
		if(auth()->user()->id == $post->user_id)
		{
			$post = Post::find($id);
			$categories = Category::get();
			$user_id = auth()->user()->id;
			$blogs = Blog::get()->where('user_id',$user_id);

			return view('posts.edit', compact('post','categories','blogs'));
		}
		else
		{
			return redirect('/post')->with('success','You do not have permission to edit it.!');
		}
	}
	public function update(Request $request, $id)
	{
		$this->validate($request, [
            'title' => 'required',
            'body' => 'required',
            'tags' => 'required',
            'category_id' => 'required',
            'blog_id' => 'required',
        ]);
		$post = Post::find($id);
		if(auth()->user()->id == $post->user_id)
		{
			$post->title = $request->input('title');
			$post->body = $request->input('body');
			$post->tags = $request->input('tags');
			$post->save();

			return redirect('/post')->with('success','Updated successfully!');
		}
		else
		{
			return "You do not have the permission to update this post!";
		}
	}
	public function destroy($id)
	{
		$post = Post::find($id);
		if(auth()->user()->id == $post->user_id)
		{
			$post->delete();
			return redirect('/post')->with('success','Deleted successfully!');	
		}
		else
		{
			return redirect('/post')->with('success','You do not have permission to delete it.!');
		}
		
	}
	
	// I could make a like controller; for simpliciy just using post controller
	// Or is there any better way to do that??

	public function like($id)
	{
		$user_id = auth()->user()->id;
		// check if liked previously

		$liked = Like::get()->where('user_id',$user_id)->where('post_id',$id);
		if($liked->isNotEmpty())
		{
			return redirect()->back()->with('success','You already liked this post.');
		}
		else
		{
			$like = new Like();
			$like->user_id = $user_id;
			$like->post_id = $id;
			$like->save();

			return redirect()->back()->with('success','You just liked this post.');
		}
	}
}
