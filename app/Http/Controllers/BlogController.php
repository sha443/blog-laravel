<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;
use App\User;

class BlogController extends Controller
{
    public function __construct()
	{
		$this->middleware('auth');
	}
    public function index()
	{
		$blogs = Blog::get();
		return view('blogs.index',compact('blogs','blogger'));
	}
	public function create()
	{
		return view('blogs.create');
	}
	public function store(Request $request)
	{
		// Blog::create($request->all());
		$this->validate($request, [
			'title'=>'required',
			'description'=>'required',
		]);
		$blog = new Blog;
		$blog->title = $request->input('title');
		$blog->description = $request->input('description');
		$blog->user_id = auth()->user()->id;
		$blog->save();

		return redirect('/blog/create')->with('success',"Blog created successfully");
	}
	public function show($id)
	{
		$blog = Blog::find($id);
		$blogger = User::find($blog->user_id)->name;
		return view('blogs.show',compact('blog','blogger'));
	}
	public function edit($id)
	{
		$blog = Blog::find($id);
		if(auth()->user()->id == $blog->user_id)
		{
			return view('blogs.edit',compact('blog'));
		}
		else
		{
			return redirect('/blog')->with('success','You do not have the permission to edit this blog!');
		}
	}
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'title'=>'required',
			'description'=>'required',
		]);
		$blog = Blog::find($id);
		if(auth()->user()->id == $blog->user_id)
		{
			
			$blog->title = $request->input('title');
			$blog->description = $request->input('description');
			$blog->save();
			return redirect('/blog')->with('success','Updated successfully!');
		}
		else
		{
			return redirect('/blog')->with('success','This blog can not be updated!');
		}
	}
	public function destroy($id)
	{
		$blog = Blog::find($id);
		if(auth()->user()->id == $blog->user_id)
		{
			$blog->delete();
			return redirect('/blog')->with('success','Deleted successfully!');
		}
		else
		{
			return redirect('/blog')->with('success','You do not have permission to delete this blog!');
		}

		
	}
}
