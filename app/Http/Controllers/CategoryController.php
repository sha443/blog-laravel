<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;

class CategoryController extends Controller
{
	public function __construct()
	{
		$this->middleware('auth');
	}
    public function index()
	{
		$categories = Category::get();
		return view('categories.index',compact('categories'));
	}
	public function create()
	{
		return view('categories.create');
	}
	public function store(Request $request)
	{
		$this->validate($request, [
			'title'=>'required',
			'description'=>'required',
		]);
		Category::create($request->all());
		// $category = new Category;
		// $category->title = $request->input('title');
		// $category->description = $request->input('description');
		// $category->save();

		return redirect('/category/create')->with('success',"Category created successfully");
	}
	public function show($id)
	{
		$category = Category::find($id);
		return view('categories.show',compact('category'));
	}
	public function edit($id)
	{
		$category = Category::find($id);
		return view('categories.edit',compact('category'));
	}
	public function update(Request $request, $id)
	{
		$this->validate($request, [
			'title'=>'required',
			'description'=>'required',
		]);
		$category = Category::find($id);
		$category->title = $request->input('title');
		$category->description = $request->input('description');
		$category->save();

		return redirect('/category')->with('success','Updated successfully!');
	}
	public function destroy($id)
	{
		$category = Category::find($id);
		$category->delete();

		return redirect('/category')->with('success','Deleted successfully!');
	}
}
