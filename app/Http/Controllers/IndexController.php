<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Views;
use App\Post;
use App\User;
use App\Category;
use App\Like;
use App\Comment;

class IndexController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
    	$posts = Post::orderBy('created_at','desc')->paginate(10);
    	foreach($posts as $post)
    	{
    		$post->user_id = User::find($post->user_id)->name;
            $post->category_id = Category::find($post->category_id)->title;
    		$count_likes = Like::get()->where('post_id',$post->id)->count();
            $post->likes = $count_likes;

            $post->comments = Comment::get()->where('post_id',$post->id)->count();
    	}
    	return view('index', compact('posts'));
    }
}
