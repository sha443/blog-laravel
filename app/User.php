<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Role;
use App\UserRole;
use App\User;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'roles' => 'array',
    ];
    public function blogs()
    {
        return $this->hasMany('App\Blog');
    }
    public function likes()
    {
        return $this->hasMany('App\Like');
    }
    public function comments()
    {
        return $this->hasMany('App\Comments');
    }
    public function setNameAttribute($value)
    {
        $this->attributes['name'] = ucfirst($value);
    }
    public function getNameAttribute()
    {
        return ucfirst($this->attributes['name']);
    }

    // Role Management
    public function addRole(string $role)
    {
        $roles = $this->getRoles();
        $roles[] = $role;
        
        $roles = array_unique($roles);
        $this->setRoles($roles);

        return $this;
    }
    public function setRoles(array $roles)
    {
        $this->setAttribute('roles', $roles);
        return $this;
    }
    public function hasRole($role)
    {
        return in_array($role, $this->getRoles());
    }
    public function hasRoles($roles)
    {
        $currentRoles = $this->getRoles();
        foreach($roles as $role) {
            if ( ! in_array($role, $currentRoles )) {
                return false;
            }
        }
        return true;
    }
    public function getRoles()
    {
        $roles = $this->getAttribute('roles');

        if (is_null($roles)) {
            $roles = [];
        }
        return $roles;
    }
}
