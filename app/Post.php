<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
    	'title', 'body', 'tags','category_id','blog_id'
    ];
    public function blog()
    {
    	return $this->belongsTo('App\Blog');
    }
    public function category()
    {
    	return $this->belongsTo('App\Category');
    }
    public function likes()
    {
        return $this->hasMany('App\Like');
    }
    public function comments()
    {
        return $this->hasMany('App\Comments');
    }
}
