@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Posts | 
                <a href="/post/create" class="btn btn-success">Create New</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <?php $i=1 ?>
                        <th>Sl.</th><th>Title</th><th>Body</th><th>Tags</th><th>Edit</th><th>Delete</th>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{$i++}}</td><td><a href="/post/{{$post->id}}">{{$post->title}}</a></td><td>{{$post->body}}</td><td>{{$post->tags}}</td>

                                @if(auth()->user()->id==$post->user_id)
                                <td><a href="/post/{{$post->id}}/edit" class="btn btn-success">Edit</a></td><td>
                                    <form method="post" action="/post/{{$post->id}}">
                                        {{ method_field('delete') }}
                                         @csrf
                                         <input type="submit" name="destroy" class="btn btn-danger" value="Delete">
                                    </form>
                                </td>
                                @else
                                    <td><a href="/post/{{$post->id}}/edit" class="btn btn-warning">Edit</a></td><td>
                                        <form method="post" action="/post/{{$post->id}}">
                                            {{ method_field('delete') }}
                                             @csrf
                                             <input type="submit" name="destroy" class="btn btn-warning" value="Delete">
                                        </form>
                                    </td>
                                @endif
                            </tr>
                        @endforeach
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
