@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Posts | 
                <a href="/post" class="btn btn-success">Back to Post</a>
                </div>
                <div class="card-body">
                    <p><h3>{{ $post->title }}</h3> 
                        <span>Posted by <b>{{$blogger}}</b> in <b>{{$blog_title}}</b> under <i>{{$category_title}}</i></span> <br>
                        <span>Posted at: {{$post->created_at}}</span>
                    </p> <hr>
                    <p>{{$post->body}}</p> <hr>
                    <p><b>Tags: </b>{{$post->tags}}</p>
                </div>
                <div class="card-footer">
                    @foreach($comments as $comment)
                    <div class="modal-body">
                        <b>{{$comment->user_id}}</b> says: {{$comment->body}}
                        <i>[at {{$comment->created_at}}]</i>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
