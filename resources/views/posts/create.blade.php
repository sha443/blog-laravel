@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Create Post | <a href="/post" class="btn btn-success">Back to Post</a></div>
                <div class="alert alert-success">
                </div>
                <div class="card-body">
                    <form method="POST" action="/post">
                        @csrf

                        <div class="form-group row">
                            <label for="Title" class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-6">
                                <input id="title" type="text" class="form-control" name="title" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Body" class="col-md-4 col-form-label text-md-right">{{ __('Body') }}</label>

                            <div class="col-md-6">
                                <textarea id="body" class="form-control"  name="body" required autofocus></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Tags" class="col-md-4 col-form-label text-md-right">{{ __('Tags') }}</label>

                            <div class="col-md-6">
                                <input id="tags" type="text" class="form-control" name="tags" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Categoy" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" name="category_id">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="Blog" class="col-md-4 col-form-label text-md-right">{{ __('Blog') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" name="blog_id">
                                    @foreach($blogs as $blog)
                                        <option value="{{$blog->id}}">{{$blog->title}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Create') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
