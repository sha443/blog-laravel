@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    
                </div>
                <div class="col-md-6">
                     @foreach($posts as $post)
                     <div class="card">
                         <div class="card-header">
                             <span class="h4"><a href="/post/{{$post->id}}">{{$post->title}}</a></span>
                         </div>
                         <div class="modal-header">
                             <span class="h6 text-left">Posted by: <b>{{$post->user_id}}</b>
                            at <i>{{$post->created_at->format('d-M-y h:ia')}}</i>
                            <br>
                            Category: {{$post->category_id}}</span>
                         </div>
                         <div class="card-body">
                            {{$post->body}} <br><br>
                            <i>Tags: {{$post->tags}}</i>
                        </div>
                        <div class="modal-footer">
                             <a href="/post/{{$post->id}}/like">   <i class="fa fa-heart"></i> Like </a> ({{$post->likes}})  |  <a href="/comment/{{$post->id}}"><i class="fa fa-comment"></i> Comment </a>({{$post->comments}})
                        </div>
                    </div>
                    <br>
                    @endforeach
                    {{$posts->links()}}                   
                </div>
            </div>

        </div>
    </div>
</div>
@endsection
