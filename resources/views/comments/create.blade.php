@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Leave a Comment | <a href="/index" class="btn btn-success">Back to post</a></div>
                <div class="alert alert-success">
                </div>
                <div class="card-body">
                    <form method="POST" action="/comment">
                        @csrf
                        <input type="text" name="post_id" value="{{$id}}" hidden>
                        <div class="form-group row">
                            <label for="Comment" class="col-md-4 col-form-label text-md-right">{{ __('Comment') }}</label>

                            <div class="col-md-6">
                                <textarea id="body" class="form-control"  name="body" required autofocus></textarea>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Post') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
