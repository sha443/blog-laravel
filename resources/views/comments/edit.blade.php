@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Post</div>
                <div class="card-body">
                    <!-- arbitraty guess route, route(update,id) -->
                    <form method="post" action="/post/{{$post->id}}">
                        {{ method_field('PUT') }}
                        @csrf

                        <!-- or -->
                        <!-- <input type="hidden" name="_method" value="PUT">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"> -->

                        <div class="form-group row">
                            <label for="Title"  class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>

                            <div class="col-md-6">
                                <input id="title" type="text" value="{{$post->title}}" class="form-control" name="title" required autofocus>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Body" class="col-md-4 col-form-label text-md-right">{{ __('Body') }}</label>

                            <div class="col-md-6">
                                <textarea id="body" class="form-control" name="body" required autofocus>{{$post->body}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="Tags"  class="col-md-4 col-form-label text-md-right">{{ __('Tags') }}</label>

                            <div class="col-md-6">
                                <input id="tags" type="text" value="{{$post->tags}}" class="form-control" name="tags" required autofocus>
                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="Categoy" class="col-md-4 col-form-label text-md-right">{{ __('Category') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" name="category_id">
                                    @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->title}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>
                         <div class="form-group row">
                            <label for="Blog" class="col-md-4 col-form-label text-md-right">{{ __('Blog') }}</label>

                            <div class="col-md-6">
                                <select class="form-control" name="blog_id">
                                    @foreach($blogs as $blog)
                                        <option value="{{$blog->id}}">{{$blog->title}}</option>
                                    @endforeach
                                </select>

                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Update') }}
                                </button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
