@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Categories | 
                <a href="/category/create" class="btn btn-success">Create New</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <?php $i=1 ?>
                        <th>Sl.</th><th>Title</th><th>Description</th><th>Edit</th><th>Delete</th>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{$i++}}</td><td><a href="/category/{{$category->id}}">{{$category->title}}</a></td><td>{{$category->description}}</td><td><a href="/category/{{$category->id}}/edit" class="btn btn-success">Edit</a></td><td>
                                    <form method="post" action="/category/{{$category->id}}">
                                        {{ method_field('delete') }}
                                         @csrf
                                         <input type="submit" name="destroy" class="btn btn-danger" value="Delete">
                                    </form>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
