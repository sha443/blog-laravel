@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Categories | 
                <a href="/category" class="btn btn-success">Back to Category</a>
                </div>
                <div class="card-body">
                    <p>{{ $category->title }}</p> <hr>
                    <p>{{$category->description}}</p>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
