@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Categories | 
                <a href="/blog" class="btn btn-success">Back to Blog</a>
                </div>
                <div class="card-body">
                    <p><h4>{{ $blog->title }}</h4> by <b>{{$blogger}}</b></p> <hr>
                    <p>{{$blog->description}}</p>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
