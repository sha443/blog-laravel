@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            
            <div class="card">
                <div class="card-header">Blogs | 
                <a href="/blog/create" class="btn btn-success">Create New</a>
                </div>
                <div class="card-body">
                    <table class="table">
                        <?php $i=1 ?>
                        <th>Sl.</th><th>Title</th><th>Description</th><th>Edit</th><th>Delete</th>
                        @foreach($blogs as $blog)
                            <tr>
                                <td>{{$i++}}</td><td><a href="/blog/{{$blog->id}}">{{$blog->title}}</a></td><td>{{$blog->description}}</td>
                            @if(auth()->user()->id == $blog->user_id)
                                <td><a href="/blog/{{$blog->id}}/edit" class="btn btn-success" >Edit</a></td><td>
                                    <form method="post" action="{{ route('destroy',$blog->id) }}">
                                        {{ method_field('delete') }}
                                         @csrf
                                         <input type="submit" name="destroy" class="btn btn-danger" value="Delete">
                                    </form>
                                </td>
                            @else
                              <td><a href="/blog/{{$blog->id}}/edit" class="btn btn-warning" >Edit</a></td><td>
                                    <form method="post" action="{{ route('destroy',$blog->id) }}">
                                        {{ method_field('delete') }}
                                         @csrf
                                         <input type="submit" name="destroy" class="btn btn-warning" value="Delete">
                                    </form>
                                </td>
                            @endif

                            </tr>
                        @endforeach
                    </table>
                   
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
