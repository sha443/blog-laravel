@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Blog</div>
                <div class="card-body">
                    <!-- arbitraty guess route, route(update,id) -->
                        <form method="post" action="{{ route('update',$blog->id) }}">
                            {{ method_field('PUT') }}
                            @csrf

                            <div class="form-group row">
                                <label for="Title"  class="col-md-4 col-form-label text-md-right">{{ __('Title') }}</label>

                                <div class="col-md-6">
                                    <input id="title" type="text" value="{{$blog->title}}" class="form-control" name="title" required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="Desctiption" class="col-md-4 col-form-label text-md-right">{{ __('Desctiption') }}</label>

                                <div class="col-md-6">
                                    <input id="description" value="{{$blog->description}}" type="textarea" class="form-control" name="description" required autofocus>
                                </div>
                            </div>


                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Update') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                </div>
            </div>
            </div>
        </div>
    </div>
</div>
@endsection
