@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">404</div>
                <br>
                <h3 class="text-center">Congratulations! you have finally found the page 404.</h3>
                <h4 class="text-center">Yeah, certainly this is the page you're looking for.</h4>
                <br>

                <br>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
