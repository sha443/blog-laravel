use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

use App\Category;

class CategoryTest extends TestCase
{
	use DatabaseMigrations;

	public function testCanInstantiateCategory()
	{
		$category = new Category;
		$this->assertEquals(get_class($category), 'App\Category');
	}
}