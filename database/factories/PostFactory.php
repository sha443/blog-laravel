<?php

use Faker\Generator as Faker;
use App\Post;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title'=>$faker->sentence,
        'body'=>$faker->text,
        'tags'=>$faker->word,
        'likes'=>0,
        'user_id'=>1,
        'blog_id'=>2,
        'category_id'=>3,
        'created_at'=>now(),
        'updated_at'=>now(),

    ];
});
