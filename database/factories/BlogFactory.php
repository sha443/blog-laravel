<?php
use App\Blog;
use Faker\Generator as Faker;

$factory->define(App\Blog::class, function (Faker $faker) {
    return [
        'title'=>$faker->name,
        'description'=>$faker->sentence,
        'user_id'=> 1,
        'created_at' => now(),
        'updated_at' => now(),
    ];
});
